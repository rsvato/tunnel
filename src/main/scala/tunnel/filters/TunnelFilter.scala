package tunnel.filters

import javax.servlet._
import http.{HttpServletResponse, HttpServletRequest}
import org.jboss.netty.bootstrap.ClientBootstrap
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory
import java.net.{InetSocketAddress, HttpURLConnection, URL}
import org.jboss.netty.channel._
import org.jboss.netty.handler.codec.http._
import java.util.concurrent.{TimeUnit, Executors}
import org.jboss.netty.buffer.ChannelBuffers

sealed trait Tunnel {
  def backendHost: String
  def uriMatch : String
  def doServe(request: HttpServletRequest, response: NotFoundWrapper)
}

abstract class TunnelFilter(override  val uriMatch: String) extends Filter with Tunnel with Logging {
  var backendHost: String = _
  def init(filterConfig: FilterConfig) {
    backendHost = filterConfig.getInitParameter(TunnelFilter.BACKEND_PARAMETER)
  }

  def doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
    val rq = request.asInstanceOf[HttpServletRequest]
    val wrapped = new NotFoundWrapper(response.asInstanceOf[HttpServletResponse], Array(404,405))
    try {
      chain.doFilter(request, wrapped)
    } finally {
      log.debug("Finish processing")
      if (wrapped.willServe)
        serve(rq, wrapped)
    }
  }

  def serve(request: HttpServletRequest, response: NotFoundWrapper) {
    log.debug("Starting tunneling")
    if (request.getRequestURI.startsWith(uriMatch)) {
      doServe(request, response)
    } else {
      response.resume()
    }
  }

  def destroy() {}
}

object TunnelFilter {
  val BACKEND_PARAMETER = "backendHost"
}



trait NettyTunnel extends Tunnel with Logging {
  val bossPool = Executors.newFixedThreadPool(5)
  val handlerPool = Executors.newFixedThreadPool(5 * 3)
  def doServe(request: HttpServletRequest, response: NotFoundWrapper) {
    val url = new URL(backendHost + "/" + request.getRequestURI)
    val bootstrap = new ClientBootstrap(new NioClientSocketChannelFactory(bossPool, handlerPool))
    bootstrap.setPipelineFactory(new TunnelPipelineFactory(request, response))
    val future = bootstrap.connect(new InetSocketAddress(url.getHost, url.getPort))
    val channel = future.awaitUninterruptibly().getChannel
    if (! future.isSuccess)
      response.resume()
    else {
      channel.write(prepareRequest(request, url))
      channel.getCloseFuture.awaitUninterruptibly(5, TimeUnit.SECONDS)
    }
  }

  def prepareRequest(req: HttpServletRequest, host: URL) : HttpRequest = {
    var uri = Option(req.getQueryString) match {
      case Some(s) => req.getRequestURI + "?" + s
      case _ => req.getRequestURI
    }
    val request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, uri);
    request.setHeader(HttpHeaders.Names.HOST, host.getHost);
    request.setHeader(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.CLOSE);
    request.setHeader(HttpHeaders.Names.ACCEPT_ENCODING, HttpHeaders.Values.GZIP);
    if (req.getContentType != null)
      request.setHeader(HttpHeaders.Names.CONTENT_TYPE, req.getContentType);
    request.setHeader("X-On-Behalf-Of", "me")
    request.setMethod(HttpMethod.valueOf(req.getMethod))
    val input = Iterator.continually(req.getInputStream.read).takeWhile(-1 != _).map(_.toByte).toArray
    val buffer = ChannelBuffers.copiedBuffer(input)
    request.setHeader(HttpHeaders.Names.CONTENT_LENGTH, buffer.readableBytes())
    request.setContent(buffer)
    request
  }
}


class TunnelPipelineFactory(val request: HttpServletRequest, val response: HttpServletResponse) extends ChannelPipelineFactory {
  def getPipeline = {
    val pipeline: ChannelPipeline = new DefaultChannelPipeline()
    pipeline.addLast("codec", new HttpClientCodec())
    pipeline.addLast("inflate", new HttpContentDecompressor)
    pipeline.addLast("handler", new OutputStreamHandler(request, response))
    pipeline
  }
}

class OutputStreamHandler(val request: HttpServletRequest, val response: HttpServletResponse)
  extends SimpleChannelUpstreamHandler with Logging {
  override def exceptionCaught(ctx: ChannelHandlerContext, e: ExceptionEvent) {
     log.error(e.getCause, "Exception during working with remote end: %s", e)
     response.resetBuffer()
     response.sendError(500, "Remote party error")
  }

  override def messageReceived(ctx: ChannelHandlerContext, e: MessageEvent) {
    val remote : HttpResponse = e.getMessage.asInstanceOf[HttpResponse]
    response.resetBuffer()
    response.setStatus(remote.getStatus.getCode)
    response.addHeader("X-Tunnelled", "true")
    import scala.collection.JavaConversions._
    val headers = remote.getHeaderNames
    for (header <- headers if header != "Connection") {
      for (vals <- remote.getHeaders(header)) {
        response.addHeader(header, vals)
      }
    }
    val content = remote.getContent
    if (content.readable()) {
      response.getOutputStream.write(content.toByteBuffer.array())
    }
    response.getOutputStream.flush()
    response.getOutputStream.close()
  }
}

trait UrlConnectionTunnel extends Tunnel with Logging {
  def doServe(request: HttpServletRequest, response: NotFoundWrapper) {
    val uri = Option(request.getQueryString) match {
      case Some(s) => request.getRequestURI + "?" + s
      case _ => request.getRequestURI
    }
    val url = new URL(backendHost + uri)
    val connection = url.openConnection().asInstanceOf[HttpURLConnection]
    connection.setRequestMethod(request.getMethod)
    var doWrite = false
    if (request.getMethod == "POST" || request.getMethod == "PUT") {
      connection.setDoOutput(true)
      doWrite = false
    }
    connection.setDoInput(true)
    connection.addRequestProperty("Connection", "close") //no keep-alive
    try {
      connection.connect()
      if (doWrite) {
        val remoteOut = connection.getOutputStream
        ResourceFilter.copyStream(request.getInputStream, remoteOut)
        remoteOut.flush()
        remoteOut.close()
      }
      val localOut = response.getOutputStream
      response.resetBuffer()
      response.setStatus(connection.getResponseCode)
      response.addHeader("X-On-Behalf-Of", "me")
      ResourceFilter.copyStream(connection.getInputStream, localOut)
      localOut.flush()
      localOut.close()
    } catch {
      case e => {
        log.error(e, "Error when tunneling request")
        response.resume()
      }
    } finally {
      connection.disconnect()
    }
  }
}




