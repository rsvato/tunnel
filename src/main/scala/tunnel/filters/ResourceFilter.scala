package tunnel.filters

import java.text.SimpleDateFormat
import java.util.{Locale, Calendar, Date}
import java.io.{OutputStream, InputStream}
import org.apache.commons.vfs.{FileContent, FileObject, VFS}
import javax.servlet._
import http.{HttpServletRequest, HttpServletResponse}


class ResourceFilter extends Filter with Logging {
  var resourceRoots: Seq[String] = _
  var config: FilterConfig = _
  val manager = VFS.getManager
  val expires: Date = {
    val cal = Calendar.getInstance()
    cal.roll(Calendar.YEAR, true)
    cal.getTime
  }

  def init(filterConfig: FilterConfig) {
    resourceRoots = filterConfig.getInitParameter("resourceRoots").split(",").map(
      s => s.replaceAll("classpath:", "res:")
    )
    log.debug("Resource roots: %s", resourceRoots)
    config = filterConfig
  }

  def writeContent(response: HttpServletResponse, f: FileObject, cache: Boolean) {
    response.resetBuffer()
    response.setStatus(HttpServletResponse.SC_OK)
    val content: FileContent = f.getContent
    val contentType = Option(config.getServletContext.getMimeType(f.getName.getBaseName))
    response.setHeader("Content-type", contentType.getOrElse("application/octet-stream"))
    val httpDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US)
    response.setHeader("Last-modified", httpDateFormat.format(new Date(content.getLastModifiedTime)))
    if (cache) {
      response.setHeader("Expires", httpDateFormat.format(expires));
      response.setHeader("Cache-control", "public, max-age=31536000")
    } else {
      response.setHeader("Cache-control", "no-cache, no-store, must-revalidate")
    }
    val stream: InputStream = content.getInputStream
    val out: OutputStream = response.getOutputStream
    ResourceFilter.copyStream(stream, out)
    out.flush()
    out.close()
    stream.close()
  }

  def serve(response: NotFoundWrapper, uri: String) {
    val (originalPath, cache) = if (uri == null || uri == "" || uri == "/")
      ("index.html", false)
    else
      (uri.replaceAll("^\\/", "").takeWhile(_ != ';'), true)
    def locateResource(path: String) = {
      resourceRoots.map(
        root => {
          val result = try {
            log.debug("Examining %s", root + path)
            Some(manager.resolveFile(root + path))
          } catch {
            case e => {
              log.debug("File %s not found", root + path)
              None
            }
          }
          result
        }).filter(! _.isEmpty).map(_.get).headOption
    }
    val result: Option[FileObject] = locateResource(originalPath) match {
      case s@Some((_)) => s
      case None => {
        locateResource(originalPath + ".html")
      }
    }
    log.debug("Results is %s", result)
    result match {
      case Some(f) if f.exists() => {
        writeContent(response, f, cache)
      }
      case _ => response.resume();
    }
  }


  def doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
    val req: HttpServletRequest = request.asInstanceOf[HttpServletRequest]
    val uri: String = req.getRequestURI
    val wrappedResponse: NotFoundWrapper = new NotFoundWrapper(response.asInstanceOf[HttpServletResponse],
      Array(404))
    try {
      log.debug("Starting to work aroud uri %s", uri)
      chain.doFilter(request, wrappedResponse)
    } finally {
      if (wrappedResponse.willServe) {
        log.debug("I'll try to serve %s", uri)
        serve(wrappedResponse, uri)
      }
      log.debug("Finish processing")
    }

  }

  def destroy() {

  }
}

object ResourceFilter {
  def copyStream(is: InputStream, os: OutputStream) {
    val buffer = new Array[Byte](4096)
    def read() {
      val count = is.read(buffer)
      if (count != -1) {
        os.write(buffer, 0, count)
        read()
      }
    }
    read()
  }

}
