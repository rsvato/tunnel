package tunnel.filters

import org.slf4j.{Logger, LoggerFactory}


trait Logging {
  protected implicit val log = new RichLogger(LoggerFactory.getLogger(this.getClass))
}

class RichLogger(val logger : Logger) {
  def info(message : String, args : Any*) {
    if (logger.isInfoEnabled)
      if (args.isEmpty)
        logger.info(message)
      else
        logger.info(message.format(args : _*))
  }

  def info(throwable : Throwable, message : String, args : Any*) {
    if (logger.isInfoEnabled)
      if (args.isEmpty)
        logger.info(message, throwable)
      else
        logger.info(message.format(args : _*), throwable)
  }

  def warn(message : String, args : Any*) {
    if (logger.isWarnEnabled)
      if (args.isEmpty)
        logger.warn(message)
      else
        logger.warn(message.format(args : _*))
  }

  def warn(throwable : Throwable, message : String, args : Any*) {
    if (logger.isWarnEnabled)
      if (args.isEmpty)
        logger.warn(message, throwable)
      else
        logger.warn(message.format(args : _*), throwable)
  }

  def error(message : String, args : Any*) {
    if (logger.isErrorEnabled)
      if (args.isEmpty)
        logger.error(message)
      else
        logger.error(message.format(args : _*))
  }

  def error(throwable : Throwable, message : String, args : Any*) {
    if (logger.isErrorEnabled)
      if (args.isEmpty)
        logger.error(message, throwable)
      else
        logger.error(message.format(args : _*), throwable)
  }

  def debug(message : String, args : Any*) {
    if (logger.isDebugEnabled)
      if (args.isEmpty)
        logger.debug(message)
      else
        logger.debug(message.format(args : _*))
  }

  def debug(throwable : Throwable, message : String, args : Any*) {
    if (logger.isDebugEnabled)
      if (args.isEmpty)
        logger.debug(message, throwable)
      else
        logger.debug(message.format(args : _*), throwable)
  }

  def trace(message : String, args : Any*) {
    if (logger.isTraceEnabled)
      if (args.isEmpty)
        logger.trace(message)
      else
        logger.trace(message.format(args : _*))
  }

  def trace(throwable : Throwable, message : String, args : Any*) {
    if (logger.isTraceEnabled)
      if (args.isEmpty)
        logger.trace(message, throwable)
  }
}


