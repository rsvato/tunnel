package tunnel.filters

import javax.servlet.http.{HttpServletResponseWrapper, HttpServletResponse}


class NotFoundWrapper(response: HttpServletResponse, codes : Array[Int] = Array(404)) extends HttpServletResponseWrapper(response) with Logging {
  def willServe = {
    log.debug("Catched status code is %d, my codes are %s", statusCode, codes)
    codes.contains(statusCode)
  }

  var statusCode: Int = _
  var message: Option[String] = None
  var callback: Unit = _

  override def setStatus(sc: Int) {
    if (checkStatus(sc, None)) {
      super.setStatus(sc)
    }
  }

  override def setStatus(sc: Int, sm: String) {
    if (checkStatus(sc, Some(sm)))
      super.setStatus(sc)
  }


  override def sendError(sc: Int, msg: String) {
    if (checkStatus(sc, Some(msg)))
      super.sendError(sc, msg)
  }

  override def sendError(sc: Int) {
    if (checkStatus(sc, None)) {
      super.sendError(sc)
    }
  }

  def resume() {
    if (!response.isCommitted) {
      message match {
        case None => response.sendError(statusCode)
        case Some(s) => response.sendError(statusCode, s)
      }
    }
  }

  private def checkStatus(sc: Int, msg: Option[String]): Boolean = {
    statusCode = sc
    message = msg
    ! codes.contains(sc)
  }
}
