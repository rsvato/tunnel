package tunnel

import filters.{UrlConnectionTunnel, NettyTunnel, ResourceFilter, TunnelFilter}
import org.eclipse.jetty.server.nio.SelectChannelConnector
import org.eclipse.jetty.server.{DispatcherType, Server}
import java.util.EnumSet
import servlet.MainServlet
import org.eclipse.jetty.servlet.{DefaultServlet, FilterHolder, ServletHolder, ServletContextHandler}

/**
 * @author ${user.name}
 */
object App {

  def main(args : Array[String]) {
    val context = new ServletContextHandler(ServletContextHandler.SESSIONS);
    context.setContextPath("/");

    val resourceFilter = new FilterHolder(new ResourceFilter)
    resourceFilter.setInitParameter("resourceRoots", "file:/Users/sreentenko/Pictures/")
    context.addFilter(resourceFilter, "/*", 0)

    val tunnelHolder = new FilterHolder(new TunnelFilter("/rest") with UrlConnectionTunnel)
    tunnelHolder.setInitParameter(TunnelFilter.BACKEND_PARAMETER, "http://localhost:4567")
    context.addFilter(tunnelHolder, "/*", 0)

    val holder = new ServletHolder(classOf[DefaultServlet])
    context.addServlet(holder, "/")

    val httpConnector = new SelectChannelConnector()
    httpConnector.setHost("localhost")
    httpConnector.setPort(8888)

    val server = new Server
    server.setConnectors(Array(httpConnector))
    server.setHandler(context)
    server.start()
    server.join()
  }

}
