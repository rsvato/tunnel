package tunnel.servlet

import javax.servlet.http.{HttpServletResponse, HttpServletRequest, HttpServlet}


class MainServlet extends HttpServlet {
  override def doGet(req: HttpServletRequest, resp: HttpServletResponse) {
    if (req.getRequestURI == "/hello")
      resp.getWriter.write("Hello");
    else
      resp.setStatus(HttpServletResponse.SC_NOT_FOUND)
  }
}
